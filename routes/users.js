var express = require('express');
var router = express.Router();

/* GET users listing. */ /* / is relative to /users */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* GET users /users/view  */ 
router.get('/view', function(req, res, next) {
  res.send('user view');
});

module.exports = router;
